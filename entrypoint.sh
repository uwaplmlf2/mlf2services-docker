#!/bin/bash
#

if [[ -z "$1" ]]; then
    # Create the configuration file from some of the
    # environment variables.
    export SVC_CONFIG=/config.json
    cat > $SVC_CONFIG <<EOF
{
    "db": {
        "host": "$DB_SERVER",
        "port": "$DB_PORT",
        "name": "$DB_NAME",
        "user": "$DB_USER",
        "password": "$DB_PASSWORD"
    },
    "mq" : {
        "host": "$MQ_SERVER",
        "user": "$MQ_USER",
        "password": "$MQ_PASSWORD"
    }
}
EOF

    # Dump all of the env variables. We must to this because
    # runit will not pass the env variables to the service
    # scripts. Each script must source this file.
    export > /etc/envvars

    # Run the service manager
    exec /usr/sbin/runsvdir-start
else
    exec "$@"
fi
