FROM debian:jessie
MAINTAINER Mike Kenney <mikek@apl.uw.edu>

# runit depends on /etc/inittab which is not present in debian:jessie
RUN touch /etc/inittab
RUN apt-get update && apt-get -y install \
        curl \
        libhdf5-dev \
        libnetcdf-dev \
        python-dev \
        python-numpy \
        python-setuptools \
        python-scipy \
        python-lxml \
        runit \
        && \
        apt-get -y autoremove && \
        apt-get clean

WORKDIR /
COPY requirements.txt /

# Prebuilt wheel for netCDF4 to avoid having to install compilers
COPY netCDF4-1.2.0-cp27-none-linux_x86_64.whl /

# Install the required Python packages
RUN easy_install pip && \
      pip install netCDF4-1.2.0-cp27-none-linux_x86_64.whl && \
      rm -f netCDF4-1.2.0-cp27-none-linux_x86_64.whl && \
      pip install -r /requirements.txt

# Copy the service scripts
COPY ./etc /etc/
# Copy the apps
COPY ./apps/* /usr/bin/
COPY entrypoint.sh /
# Service config file
COPY config.json /

# Base directory for the service script logs
RUN mkdir -p /var/log/mlf2
VOLUME ["/var/log/mlf2"]

ENV AMQP_SERVER=mlf2data.apl.uw.edu \
    DB_SERVER=mlf2data.apl.uw.edu \
    DB_PORT=5984 \
    DB_NAME=mlf2db \
    LOGDIR=/var/log/mlf2

ENTRYPOINT ["/entrypoint.sh"]
