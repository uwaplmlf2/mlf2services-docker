#!/usr/bin/env python
#
# Monitor the MLF2 messages database for changes and publish all new
# messages to the message-queue.
#
import couchdb
import pika
import time
try:
    import simplejson as json
except ImportError:
    import json
import logging
from optparse import OptionParser

BP = pika.BasicProperties


def timestamp():
    return long(time.time() * 1000000L)


class MessageQueue(object):
    """
    MLF2 Message Queue
    """
    def __init__(self, host, credentials):
        self.logger = logging.getLogger('mq')
        self.logger.info('Connecting to message-queue at %s', host)
        creds = pika.PlainCredentials(*credentials)
        params = pika.ConnectionParameters(host=host,
                                           virtual_host='mlf2',
                                           credentials=creds)
        self.conn = pika.BlockingConnection(params)
        self.channel = self.conn.channel()
        self.channel.exchange_declare(exchange='commands',
                                      type='topic',
                                      durable=True)

    def __enter__(self):
        return self

    def __exit__(self, *args):
        self.channel.close()
        return False

    def publish(self, float_id, body, headers=None, reply_to=None):
        """
        Publish a message for a float
        """
        self.logger.debug('float-%d: %s', float_id, body)
        qname = 'float-%d.commands' % float_id
        self.channel.queue_declare(queue=qname,
                                   durable=True,
                                   exclusive=False,
                                   auto_delete=False)
        self.channel.queue_bind(exchange='commands',
                                queue=qname,
                                routing_key=qname)
        self.channel.basic_publish(exchange='commands',
                                   routing_key=qname,
                                   body=body,
                                   properties=BP(delivery_mode=2,
                                                 headers=headers,
                                                 timestamp=timestamp(),
                                                 content_type='text/plain',
                                                 reply_to=reply_to))

    def cancel(self, float_id, msg_id):
        """
        Publish a cancellation notice for a queued message.
        """
        self.logger.info('float-%d: Cancelling %s', float_id, msg_id)
        qname = 'float-%d.cancellation' % float_id
        self.channel.queue_declare(queue=qname,
                                   durable=True,
                                   exclusive=False,
                                   auto_delete=False)
        self.channel.queue_bind(exchange='commands',
                                queue=qname,
                                routing_key=qname)
        self.channel.basic_publish(exchange='commands',
                                   routing_key=qname,
                                   body=json.dumps([msg_id]),
                                   properties=BP(
                                       delivery_mode=2,
                                       timestamp=timestamp(),
                                       content_type='application/json'))


def publish_message(msg, mq):
    """
    Publish each command from the message to the message queue.
    """
    headers = {'db_id': str(msg['_id']),
               'comment': str(msg['comment'])}
    for cmd, args in msg['contents'][:-1]:
        body = ' '.join([cmd] + args)
        mq.publish(msg['floatid'], str(body), headers=headers,
                   reply_to=str(msg['sender']))

    # The final command has a different header
    headers['eot'] = 'yes'
    cmd, args = msg['contents'][-1]
    body = ' '.join([cmd] + args)
    mq.publish(msg['floatid'], str(body), headers=headers,
               reply_to=str(msg['sender']))


def handle_change(db, change, cfg):
    """
    Handle a CouchDB change event
    """
    # We open a new connection to the AMQP broker each time. This is somewhat
    # inefficient but simpler in that we don't need to check the status of the
    # connection and implement a reconnection strategy.
    with MessageQueue(str(cfg['host']),
                      (str(cfg['user']), str(cfg['password']))) as mq:
        logging.debug('%r', change)
        doc = db[change['id']]
        if 'deleted' in change:
            return
        if doc.get('state') == 'cancelled':
            try:
                mq.cancel(doc['floatid'], change['id'])
            except Exception:
                logging.exception('Cannot cancel (%r)', doc)
            else:
                del db[change['id']]
        elif doc.get('state') == 'ready':
            try:
                publish_message(doc, mq)
            except Exception:
                logging.exception('Cannot queue (%r)', doc)
            else:
                doc['state'] = 'queued'
                doc['t_queued'] = int(time.time())
                db[change['id']] = doc


def monitor(cfg, since=0):
    if 'user' in cfg['db']:
        url = 'http://%s:%s@%s:%d' % (cfg['db']['user'],
                                      cfg['db']['password'],
                                      cfg['db']['host'],
                                      cfg['db']['port'])
    else:
        url = 'http://%s:%d' % (cfg['db']['host'],
                                cfg['db']['port'])

    server = couchdb.client.Server(url)
    db = server[cfg['db']['name']]

    logging.info('Starting monitor')
    for result in db.changes(feed='continuous', heartbeat=5000, since=since,
                             filter='messaging/messages'):
        handle_change(db, result, cfg['mq'])


def main():
    """
    %prog [options] cfgfile

    Monitor the MLF2 database for new messages and publish them to
    the float message queues.
    """
    parser = OptionParser(usage=main.__doc__)
    parser.set_defaults(verbose=False)
    parser.add_option('-v', '--verbose',
                      action='store_true',
                      dest='verbose',
                      help='enable more verbose logging')
    opts, args = parser.parse_args()

    logging.basicConfig(level=(opts.verbose and logging.DEBUG or logging.INFO),
                        format='%(levelname)-8s %(message)s')

    cfg = dict()
    try:
        cfg = json.load(open(args[0], 'r'))
    except IndexError:
        parser.error('Missing configuration file')
    except Exception:
        logging.exception('Invalid configuration file')

    monitor(cfg)

if __name__ == '__main__':
    main()
