#!/usr/bin/env python
#
# Monitor the MLF2 Message Queue for status messages and data files and
# store them in the MLF2 CouchDB
#
import os
import csv
import re
import pika
import couchdb
import zlib
import logging
import socket
import time
try:
    import simplejson as json
except ImportError:
    import json
try:
    from lxml import etree
except ImportError:
    from xml.etree import ElementTree as etree
from functools import partial
from optparse import OptionParser
from datetime import datetime, timedelta
from dateutil.tz import tzutc
from cStringIO import StringIO
from tempfile import mkstemp
from netCDF4 import Dataset


BP = pika.BasicProperties


class MqInterface(object):
    """
    Class to manage the asynchronous setup process for the AMQP message queue
    interface.
    """
    def __init__(self, host, credentials, handlers=None):
        self.channel = None
        self.conn = None
        self.consumer_tag = None
        self.handlers = handlers or []
        creds = pika.PlainCredentials(*credentials)
        self.params = pika.ConnectionParameters(host=host,
                                                virtual_host='mlf2',
                                                credentials=creds,
                                                heartbeat_interval=300)

    def start(self):
        self.conn = pika.SelectConnection(
            self.params,
            on_open_callback=self.on_connection_open)
        self.conn.set_backpressure_multiplier(10000)
        self.conn.ioloop.start()

    def stop(self):
        self.conn.close()
        self.conn.ioloop.start()
        self.channel = None

    @property
    def connection(self):
        return self.conn

    def on_connection_open(self, conn):
        logging.info('Connected to server')
        self.conn = conn
        conn.channel(self.on_channel_open)

    def on_channel_open(self, ch):
        self.channel = ch
        self.channel.exchange_declare(callback=self.on_exchange_declared,
                                      exchange='data',
                                      type='topic',
                                      durable=True)

    def on_exchange_declared(self, frame):
        logging.info('Exchange declared')
        for key, func in self.handlers:
            kwds = {'routing_key': key, 'func': func}
            if ('#' in key) or ('*' in key):
                self.channel.queue_declare(
                    callback=partial(self.on_queue_declared, **kwds),
                    exclusive=True)
            else:
                # Using an existing queue. No need to bind.
                kwds = {'qname': key, 'func': func}
                self.channel.queue_declare(
                    queue=key,
                    durable=True,
                    callback=partial(self.on_queue_bound, **kwds),
                    exclusive=False)

    def on_queue_declared(self, frame, routing_key='', func=None):
        qname = frame.method.queue
        logging.info('Queue %s declared', qname)
        kwds = {'func': func, 'qname': qname}
        self.channel.queue_bind(callback=partial(self.on_queue_bound, **kwds),
                                exchange='data',
                                queue=qname,
                                routing_key=routing_key)

    def on_queue_bound(self, frame, qname='', func=None):
        logging.info('Queue %s bound', qname)
        if qname.startswith('amq.gen'):
            no_ack = True
        else:
            no_ack = False
        self.channel.basic_consume(func, queue=qname, no_ack=no_ack)

sexp_re = re.compile('\([a-z]+ #([0-9a-f]+)#')


def parse_sexp(infile):
    """
    Return a dictionary describing an MLF2 S-exp format
    data file.
    """
    t_start = None
    t_stop = None
    nrecs = 0
    t = 0
    try:
        if isinstance(infile, str):
            infile = open(infile, 'r')
        for line in infile:
            m = sexp_re.match(line)
            if m:
                nrecs += 1
                t = int(m.group(1), 16)
                if t_start is None:
                    t_start = t
                    t_stop = t_start
        t_stop = t
    except (IOError, StopIteration):
        pass
    return {
        't_start': t_start,
        't_stop': t_stop,
        'nrecs': nrecs,
        'filetype': 'sexp'
    }


def parse_csv(infile):
    """
    Return a dictionary describing an MLF2 CSV data file.
    """
    if isinstance(infile, str):
        rdr = csv.DictReader(open(infile, 'r'))
    else:
        rdr = csv.DictReader(infile)
    row = rdr.next()
    t_start = int(row['time'])
    t_stop = t_start
    records = 1
    for row in rdr:
        records += 1
    t_stop = int(row['time'])
    d = {'t_start': t_start, 't_stop': t_stop, 'nrows': records,
         'columns': rdr.fieldnames, 'filetype': 'csv'}
    return d


def parse_netcdf(filename):
    """
    Return a dictionary describing an MLF2 netcdf data file.
    """
    d = {}
    nc = Dataset(filename, 'r')
    if 'time' in nc.variables:
        t_start = nc.variables['time'][0]
        t_stop = t_start
        nrecs = len(nc.variables['time'])
        for i in xrange(nrecs - 1, -1, -1):
            if nc.variables['time'][i] > 0:
                t_stop = nc.variables['time'][i]
                break
        d['t_start'] = int(t_start)
        d['t_stop'] = int(t_stop)

    dims = {}
    for name, dim in nc.dimensions.items():
        dims[name] = len(dim)
    d['dimensions'] = dims

    variables = {}
    for name in nc.variables:
        v = {}
        v['dimensions'] = nc.variables[name].dimensions
        v['shape'] = nc.variables[name].shape
        if hasattr(nc.variables[name], 'units'):
            v['units'] = nc.variables[name].units
        if hasattr(nc.variables[name], 'long_name'):
            v['long_name'] = nc.variables[name].long_name
        variables[name] = v
    d['variables'] = variables
    d['filetype'] = 'netcdf'
    nc.close()
    return d

epoch = datetime(1970, 1, 1, 0, 0, 0).replace(tzinfo=tzutc())


def total_seconds(td):
    return (td.microseconds + (td.seconds + td.days * 24 * 3600) * 1e6) / 1e6


def unix_time(dt):
    return int(total_seconds(dt - epoch))


def parse_datetime(date_str):
    if date_str[-1] == ':':
        date_str = date_str[:-1]
    dt = datetime.strptime(date_str, '%Y-%m-%d %H:%M:%S')
    return unix_time(dt.replace(tzinfo=tzutc()))


def parse_syslog(infile):
    """
    Return a dictionary describing an MLF2 system-log file.
    """
    t_start = None
    t_stop = None
    nrecs = 0
    try:
        if isinstance(infile, str):
            infile = open(infile, 'r')
        line = infile.next()
        f = line.split()
        t_start = parse_datetime('%s %s' % (f[0], f[1]))
        nrecs = 1
        for line in infile:
            nrecs += 1
        f = line.split()
        t_stop = parse_datetime('%s %s' % (f[0], f[1]))
    except (IOError, StopIteration):
        pass
    d = dict()
    d['t_start'] = t_start
    d['t_stop'] = t_stop
    d['nrecs'] = nrecs
    d['filetype'] = 'syslog'
    return d


def parse_listing(infile):
    line = infile.next()
    t_start = parse_datetime(line.strip())
    return {'filetype': 'listing', 't_start': t_start, 't_stop': t_start}

file2doc = {'application/x-netcdf': parse_netcdf,
            'text/x-syslog': parse_syslog,
            'text/csv': parse_csv,
            'text/x-sexp': parse_sexp,
            'text/x-listing': parse_listing}


def add_nc_attrs(filename, **attrs):
    """
    Add global attributes to a netCDF file.
    """
    nc = Dataset(filename, 'a')
    for k, v in attrs.items():
        setattr(nc, k, v)
    nc.close()


def store_data_file(db, ch, method, properties, body):
    """
    Pull a message from the files queue. The file contents are stored
    in the CouchDB as an attachment.
    """

    def _decode(s, enc):
        """
        Optionally decode the gzip-compressed message body
        """
        if enc == 'gzip':
            # Basic gzip header length
            hdrlen = 10
            # Null-terminated filename is appended to the header if the FNAME
            # flag is set.
            if (ord(s[3]) & 0x08) == 0x08:
                namelen = s[hdrlen:].find('\x00')
                hdrlen = hdrlen + namelen + 1
            return zlib.decompress(s[hdrlen:], -zlib.MAX_WBITS)
        return s

    services = {'application/x-netcdf': 'env_plot',
                'text/x-sexp': 'sexp_convert'}
    headers = properties.headers
    filename = headers.get('filename')
    floatid = headers.get('floatid')
    enc = properties.content_encoding
    mimetype = properties.content_type
    t = time.time()
    doc = {'t_start': t, 't_stop': t}
    logging.debug('File message from float %d', floatid)
    try:
        contents = _decode(body, enc)
        if mimetype == 'application/x-netcdf':
            # The netcdf parser needs a filename rather than a file-like object
            # so we need to write the contents to a temporary file.
            fd, filepath = mkstemp()
            os.write(fd, contents)
            os.close(fd)
            add_nc_attrs(filepath, floatid=floatid)
            doc = parse_netcdf(filepath)
            os.unlink(filepath)
        elif mimetype == 'image/jpeg':
            # Snapshots have the day/time encoded in the filename
            m = re.match(r'(H|L)(\d{3})(\d{2})(\d{2})\.jpg', filename, re.I)
            if m:
                yday = int(m.group(2), 10)
                hour = int(m.group(3), 10)
                minute = hour * 60 + int(m.group(4), 10)
                now = datetime.utcnow()
                # Simple heuristic to determine whether the snapshot is from
                # this year or last year.
                td = now - datetime(now.year, 1, 1)
                if td.days >= yday:
                    d = datetime(now.year, 1, 1) + timedelta(days=yday,
                                                             minutes=minute)
                else:
                    d = datetime(now.year - 1, 1, 1) + timedelta(
                        days=yday,
                        minutes=minute)
                doc['t_start'] = unix_time(d.replace(tzinfo=tzutc()))
                doc['t_stop'] = doc['t_start']
        else:
            handler = file2doc.get(mimetype)
            if handler:
                doc = handler(StringIO(contents))
    except Exception:
        logging.exception('Error parsing %r', filename)
        ch.basic_reject(delivery_tag=method.delivery_tag, requeue=False)
        return
    doc['type'] = 'data'
    doc['filename'] = filename
    doc['floatid'] = int(floatid)
    doc['t_recv'] = properties.timestamp
    logging.debug('New document: %s', repr(doc))
    # Store the JSON document describing the file then add the file contents
    # as an attachment
    try:
        doc_id, doc_rev = db.save(doc)
        logging.info('New data file %s/%s', doc_id, filename)
        doc.update({'_id': doc_id, '_rev': doc_rev})
        db.put_attachment(doc, contents, filename=filename,
                          content_type=mimetype)
        ch.basic_ack(delivery_tag=method.delivery_tag)
    except socket.error:
        logging.exception('Cannot access CouchDB server')
        ch.basic_reject(delivery_tag=method.delivery_tag, requeue=True)
    except Exception:
        logging.exception('Cannot store %s from float-%d', filename,
                          doc['floatid'])
        ch.basic_reject(delivery_tag=method.delivery_tag, requeue=False)
    else:
        # See if the file contents should be passed to any additional services.
        service = services.get(mimetype)
        if service:
            ref = str(doc['_id'])
            headers['db_id'] = ref
            qname = 'float.file.processed' % floatid
            ch.basic_publish(exchange='data',
                             routing_key=service,
                             properties=BP(reply_to=str(qname),
                                           correlation_id=ref,
                                           content_type=mimetype,
                                           headers=headers),
                             body=str(contents))


def add_data_attachment(db, ch, method, properties, body):
    """
    Add an attachment to an existing data document.
    """
    headers = properties.headers
    filename = headers.get('filename')
    mimetype = properties.content_type
    db_id = headers.get('db_id')
    error = headers.get('error')
    if db_id and not error:
        try:
            doc = db[db_id]
            db.put_attachment(doc, body, filename=filename,
                              content_type=mimetype)
            logging.info('Attached %s to %s', filename, db_id)
        except socket.error:
            logging.exception('Cannot access CouchDB server')
        except Exception:
            logging.exception('Cannot store attachment %s to %s',
                              filename, db_id)


def parse_status_date(s):
    return datetime.strptime(s, '%Y-%m-%d %H:%M:%S').replace(
        tzinfo=tzutc()).isoformat()


def parse_gps(elem):
    d = dict()
    d['lat'], d['lon'] = [float(e) for e in elem.text.split('/')]
    d['nsat'] = int(elem.get('nsats'))
    return d


def convert_xml_status(root):
    """
    Convert an MLF2 XML status message to a dictionary
    """
    d = {}
    if root.tag == 'status':
        d['mode'] = 'mission'
    else:
        d['mode'] = root.tag
    d['timestamp'] = parse_status_date(root.findtext('date'))
    d['gps'] = parse_gps(root.find('gps'))
    sens = {
        'ipr': float(root.findtext('ipr')),
        'rh': float(root.findtext('rh')),
        'voltage': [int(v) for v in root.findtext('v').split('/')]
    }
    d['sensors'] = sens
    d['irsq'] = int(root.findtext('irsq'))
    d['piston_error'] = int(root.findtext('ep'))
    d['freespace'] = int(root.findtext('df'))
    alerts = set()
    for ele in root.findall('alert'):
        alerts.add(ele.text)
    d['alerts'] = list(alerts)
    return d


def float_group(db, floatid):
    try:
        doc_id = 'apl.uw.mlf2:%d' % floatid
        doc = db[doc_id]
        return doc.get('group')
    except Exception:
        return None


def update_float(db, floatid, dt):
    """
    Update the last_login attribute for a float entry in the database.
    """
    try:
        doc_id = 'apl.uw.mlf2:%d' % floatid
        doc = db[doc_id]
        if isinstance(dt, datetime):
            doc['last_login'] = dt.strftime('%Y-%m-%dT%H:%M:%S+00:00')
        else:
            doc['last_login'] = dt
        db[doc_id] = doc
    except Exception:
        logging.exception('Cannot update float status')


def store_status(db, ch, method, properties, body):
    """
    Pull a message from the status queue and store it in the CouchDB
    """
    headers = properties.headers
    floatid = headers.get('floatid')
    logging.debug('Status message from float %d', floatid)
    try:
        if properties.content_type == 'application/xml':
            doc = convert_xml_status(etree.fromstring(body))
        else:
            doc = json.loads(body)
    except Exception:
        logging.exception('Cannot parse status message %r', body)
        ch.basic_reject(delivery_tag=method.delivery_tag, requeue=False)
        return
    doc['floatid'] = floatid
    doc['type'] = 'status'
    doc['t_recv'] = properties.timestamp
    try:
        doc_id, _ = db.save(doc)
        logging.info('New status message %s', doc_id)
    except socket.error:
        logging.exception('Cannot save status message')
        ch.basic_reject(delivery_tag=method.delivery_tag, requeue=True)
    except Exception:
        logging.exception('Cannot store status message %r', body)
        ch.basic_reject(delivery_tag=method.delivery_tag, requeue=False)
    else:
        ch.basic_ack(delivery_tag=method.delivery_tag)
        update_float(db, floatid, doc['timestamp'])
        headers['db_id'] = str(doc['_id'])
        group = float_group(db, floatid)
        if group:
            headers['group'] = group
            rk = group + '.status.processed'
        else:
            rk = 'float-%d.status.processed' % floatid
        ch.basic_publish(exchange=method.exchange,
                         routing_key=rk,
                         properties=BP(
                             content_type='application/json',
                             headers=headers),
                         body=json.dumps(doc))


def mainloop(cfg, opts):
    logging.basicConfig(level=(opts.verbose and logging.DEBUG or logging.INFO),
                        format='%(levelname)-8s %(message)s')

    if 'user' in cfg['db']:
        url = 'http://%s:%s@%s:%d' % (cfg['db']['user'],
                                      cfg['db']['password'],
                                      cfg['db']['host'],
                                      cfg['db']['port'])
    else:
        url = 'http://%s:%d' % (cfg['db']['host'],
                                cfg['db']['port'])

    logging.debug('Connecting to CouchDB %s', url)
    server = couchdb.client.Server(url)
    db = server[cfg['db']['name']]
    logging.debug('Found database: %s', db.info())

    # List of routing keys and corresponding message consumer functions
    handlers = [
        ('float.status', partial(store_status, db)),
        ('float.file', partial(store_data_file, db)),
        ('float.file.processed', partial(add_data_attachment, db))
    ]

    mq = MqInterface(str(cfg['mq']['host']),
                     (str(cfg['mq']['user']), str(cfg['mq']['password'])),
                     handlers=handlers)
    try:
        mq.start()
    except KeyboardInterrupt:
        logging.info('Exiting ...')
    except Exception:
        logging.exception('Uncaught exception')
    finally:
        mq.stop()


def main():
    """
    %prog [options] cfgfile

    Run in the background, monitor the MLF2 message queue for data messages
    from the floats, and store them in a CouchDB database.
    """
    parser = OptionParser(usage=main.__doc__)
    parser.set_defaults(verbose=False, logfile=None, background=True)
    parser.add_option('-v', '--verbose',
                      action='store_true',
                      dest='verbose',
                      help='enable more verbose logging')
    opts, args = parser.parse_args()

    cfg = dict()
    try:
        cfg = json.load(open(args[0], 'r'))
    except IndexError:
        parser.error('Missing configuration file')
    except Exception as e:
        parser.error('Invalid configuration file: %s (%s)' % (args[0],
                                                              repr(e)))

    mainloop(cfg, opts)

if __name__ == '__main__':
    main()
