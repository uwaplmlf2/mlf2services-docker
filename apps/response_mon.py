#!/usr/bin/env python
#
# Monitor the MLF2 message queue for float responses and store them in the
# database.
#
import couchdb
import pika
try:
    import simplejson as json
except ImportError:
    import json
import logging
from optparse import OptionParser
from functools import partial


def mq_setup(host, credentials):
    creds = pika.PlainCredentials(*credentials)
    params = pika.ConnectionParameters(host=host,
                                       virtual_host='mlf2',
                                       credentials=creds,
                                       heartbeat_interval=300)
    conn = pika.BlockingConnection(params)
    channel = conn.channel()
    channel.exchange_declare(exchange='commands',
                             type='topic',
                             durable=True)
    # Declare and bind input queue
    result = channel.queue_declare(exclusive=True)
    qname = result.method.queue
    channel.queue_bind(exchange='commands',
                       queue=qname,
                       routing_key='*.responses')
    # Declare output queue
    channel.queue_declare(queue='email_reply', durable=True)

    return conn, channel, qname


def publish_reply(ch, exchange, response, headers):
    """
    Send the completed response (reply) to the email task.
    """
    ts = long(response['t_received'] * 1000000L)
    props = pika.BasicProperties(content_type='text/plain',
                                 timestamp=ts,
                                 reply_to=response['notify'],
                                 delivery_mode=2,
                                 headers=headers)
    lines = []
    for row in response['contents']:
        lines.append('>' + str(row['q']))
        lines.append(str(row['a']))
    ch.basic_publish(exchange=exchange,
                     routing_key='email_reply',
                     properties=props,
                     body='\n'.join(lines))


def update_response(db, ch, method, properties, body):
    """
    Process a float response as follows:

    - extract the corresponding message ID from the header
    - use the ID to retrieve the message document from the database
    - use the ID to retrieve the partial response document if it exists
      otherwise create a new one.
    - append the float response to the response document
    - if the EOT header is present, the response is complete. Set the message
      document state to 'sent' and save the timestamp to the response
      document.
    """
    logger = logging.getLogger('mq')
    headers = properties.headers
    db_id = headers.get('db_id')
    if db_id and db_id in db:
        msg = db[db_id]
    else:
        if db_id:
            logger.error('Invalid message ID: %s', db_id)
        ch.basic_reject(delivery_tag=method.delivery_tag, requeue=False)
        return
    # Initial structure of the response document
    response = {'type': 'response',
                'ref': db_id,
                't_received': 0,
                'floatid': headers['floatid'],
                'notify': properties.reply_to,
                'contents': []}
    # Check the database for a partial response
    response_id = None
    for result in db.view('messaging/pending', key=db_id):
        response_id = result.id
        response = db[result.id]
    # Add the new contents from the queue
    response['contents'].append(json.loads(body))
    if response_id is None:
        response_id, _ = db.save(response)
        logger.info('New response document: %s', response_id)

    # An EOT header signals that we are done with this message.
    # Mark it as sent and mark the response as complete by setting
    # the timestamp
    if headers.get('eot'):
        logger.info('Message delivery complete: %s', db_id)
        t = properties.timestamp//1000000
        response['t_received'] = t
        msg['state'] = 'sent'
        msg['ref'] = response_id
        msg['t_delivered'] = t
        try:
            publish_reply(ch, '', response,
                          {'floatid': headers['floatid'],
                           'comment': str(msg.get('comment'))})
        except Exception as e:
            logger.error('Cannot publish reply (%s)', str(e))
        # If the message document is "sticky" make a copy which will
        # be redelivered during the next communication session.
        if msg['sticky']:
            new_msg = dict(msg)
            del new_msg['_id']
            del new_msg['_rev']
            new_msg['state'] = 'ready'
            doc_id, doc_rev = db.save(new_msg)
            logger.info('New message document: %s', doc_id)
    else:
        msg['state'] = 'delivery'

    db[db_id] = msg
    db[response_id] = response
    ch.basic_ack(delivery_tag=method.delivery_tag)


def mainloop(cfg, opts):
    logging.basicConfig(level=(opts.verbose and logging.DEBUG or logging.INFO),
                        format='%(levelname)-8s %(message)s')

    if 'user' in cfg['db']:
        url = 'http://%s:%s@%s:%d' % (cfg['db']['user'],
                                      cfg['db']['password'],
                                      cfg['db']['host'],
                                      cfg['db']['port'])
    else:
        url = 'http://%s:%d' % (cfg['db']['host'],
                                cfg['db']['port'])

    server = couchdb.client.Server(url)
    db = server[cfg['db']['name']]

    conn, ch, qname = mq_setup(str(cfg['mq']['host']),
                               (str(cfg['mq']['user']),
                                str(cfg['mq']['password'])))
    logging.info('Waiting for messages')
    ch.basic_consume(partial(update_response, db),
                     queue=qname,
                     no_ack=False)
    try:
        ch.start_consuming()
    except KeyboardInterrupt:
        logging.info('Exiting ...')
    except Exception:
        logging.exception('Uncaught exception')
    finally:
        ch.close()


def main():
    """
    %prog cfgfile

    Monitor the MLF2 message queue for response messages from the
    floats and store them in a CouchDB database.
    """
    parser = OptionParser(usage=main.__doc__)
    parser.set_defaults(verbose=False)
    parser.add_option('-v', '--verbose',
                      action='store_true',
                      dest='verbose',
                      help='enable more verbose logging')
    opts, args = parser.parse_args()

    cfg = dict()
    try:
        cfg = json.load(open(args[0], 'r'))
    except IndexError:
        parser.error('Missing configuration file')
    except Exception as e:
        parser.error('Invalid configuration file: %s (%s)' % (args[0],
                                                              repr(e)))
    mainloop(cfg, opts)

if __name__ == '__main__':
    main()
